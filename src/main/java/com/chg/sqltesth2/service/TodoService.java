package com.chg.sqltesth2.service;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.chg.sqltesth2.model.Todo;
import com.chg.sqltesth2.repository.TodoRepository;

import reactor.core.publisher.Flux;

@Service
public class TodoService {

	private TodoRepository todoRepository;
	
	public TodoService(TodoRepository todoRepository) {
		this.todoRepository=todoRepository;
	}
	
	@PostConstruct
	public void init() {
	}
	
	public Flux<Todo> getAllTodos() {
		return todoRepository.findAll();
	}
}
