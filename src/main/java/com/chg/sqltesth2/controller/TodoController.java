package com.chg.sqltesth2.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.chg.sqltesth2.service.TodoService;

import reactor.core.publisher.Mono;

@Controller
public class TodoController {
	private TodoService todoService;

	public TodoController(TodoService todoService) {
		this.todoService=todoService;
	}
	@GetMapping("/todos")
	public Mono<String> getAllTodos() {
		todoService.getAllTodos().subscribe(t -> {
			System.out.println("** todo: " + t.getTodo());
		});
		return Mono.just("Sucess");
	}
}
