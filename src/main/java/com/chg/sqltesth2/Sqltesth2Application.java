package com.chg.sqltesth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sqltesth2Application {

	public static void main(String[] args) {
		SpringApplication.run(Sqltesth2Application.class, args);
	}

}
