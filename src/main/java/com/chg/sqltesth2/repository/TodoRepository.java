package com.chg.sqltesth2.repository;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

import com.chg.sqltesth2.model.Todo;

@Repository
public interface TodoRepository extends ReactiveCrudRepository<Todo, Long> {

}
